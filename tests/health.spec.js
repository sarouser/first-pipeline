const chai = require('chai');
const dirtyChai = require('dirty-chai');

const { expect } = chai;
chai.use(dirtyChai);

describe('Test /users', () => {
  describe('Test that succeeds', () => {
    it('trueConstant should be okay', () => {
      const trueConstant = true;
      expect(trueConstant).to.be.true();
    });
  });
});

describe('Test /users', () => {
  describe('Another test but this time it fails!', () => {
    it('trueConstant should be okay', () => {
      const trueConstant = false;
      expect(trueConstant).to.be.false();
    });
  });
});
